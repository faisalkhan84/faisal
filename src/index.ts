/*
 * Example smart contract written in JavaScript
 *
 */
import {
  NearContract,
  NearBindgen,
  near,
  call,
  view,
  UnorderedMap,
  Vector,
  LookupMap,
} from "near-sdk-js";

class UserProductsAdded {
  productId: string;
  orders: string[];
}

class UserAddress {
  addressType: string;
  address: string;
  postCode: string;
  city: string;
}

class UserGroups {
  groupId: string;
  groupName: string;
  inviteStatus: string;
}

class User {
  accountId: string;
  productsAdded: UserProductsAdded[];
  myOrders: string[];
  firstName: string;
  lastName: string;
  email: string;
  mobileNumber: string;
  profileImage: string;
  address: UserAddress[];
  favoriteProducts: string[];
  uniqueToken: string;
  isRenter: boolean;
  userGroups: UserGroups[];
}

class ContractResponse {
  message: string;
  success: boolean;
}

@NearBindgen
class UserContract extends NearContract {
  userDetails: UnorderedMap;
  constructor() {
    super();
    this.userDetails = new UnorderedMap("user-uid-1");
  }

  // Override the deserializer to load vector from chain
  // deserialize() {
  //   super.deserialize();
  //   this.userDetails = Object.assign(new LookupMap(), this.userDetails);
  // }

  default() {
    return new UserContract();
  }

  //contract for storing userDetails
  @call
  storeUserDetails({
    accountId,
    firstName,
    lastName,
    email,
    mobileNumber,
    profileImage,
    uniqueToken,
    isRenter,
  }: {
    accountId: string;
    firstName: string;
    lastName: string;
    email: string;
    mobileNumber: string;
    profileImage: string;
    uniqueToken: string;
    isRenter: boolean;
  }): ContractResponse {
    //it will contain boolean value by fetching the accountId it will return true or false
    let accountExists = this.userDetails.get(accountId);
    //if account does not exists then we can store userDetails
    if (accountExists == null) {
      //set the userDetails with key value where key is accountId and value is userDetails
      this.userDetails.set(accountId, {
        accountId: accountId,
        productsAdded: [],
        myOrders: [],
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNumber: mobileNumber,
        profileImage: profileImage,
        address: [],
        favoriteProducts: [],
        uniqueToken: uniqueToken,
        isRenter: isRenter,
        userGroups: [],
      });
      //return the response message
      return {
        message: "Stored User Details successfully",
        success: true,
      };
    } else {
      //if the storeDetails exists
      return {
        message: "User Details already Exists",
        success: false,
      };
    }
  }

  //contract for updating userDetails
  @call
  updateUserDetails({
    accountId,
    firstName,
    lastName,
    email,
    mobileNumber,
    profileImage,
  }: {
    accountId: string;
    firstName: string;
    lastName: string;
    email: string;
    mobileNumber: string;
    profileImage: string;
  }): ContractResponse {
    //it will contain boolean value by fetching the accountId it will return true or false
    let accountExists = this.userDetails.get(accountId);

    //if account exists then we can update userDetails
    if (accountExists) {
      //this will return the userDetails by getting through the accountId
      let userInfo: any = this.userDetails.get(accountId);
      //set the userDetails with key value where key is accountId and value as userDetails here we will add the current changes
      this.userDetails.set(accountId, {
        accountId: accountId,
        productsAdded: userInfo.productsAdded,
        myOrders: userInfo.myOrders,
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNumber: mobileNumber,
        profileImage: profileImage,
        address: userInfo.address,
        favoriteProducts: userInfo.favoriteProducts,
        uniqueToken: userInfo.uniqueToken,
        isRenter: userInfo.isRenter,
        userGroups: userInfo.userGroups,
      });
      //return the success response message
      return {
        message: "Updated User Details successfully",
        success: true,
      };
    } else {
      //if the userDetails does not exists return the response
      return {
        message: "User Details not present in blockchain",
        success: false,
      };
    }
  }

  //contract for getting userDetails
  @view
  getUser({ accountId }: { accountId: string }): User | null {
    //it will contain boolean value by fetching the accountId it will return true or false
    let accountExists = this.userDetails.get(accountId);
    //if account exists then we can get userDetails
    if (accountExists) {
      let details: any = this.userDetails.get(accountId);
      return details;
    } else {
      //if the userDetails does not exists return null
      return null;
    }
  }

  // contract for getting allUsers
  @view
  getAllUsers(): any {
    //it will return the values from startIndex to endIndex
    let result: any = [];

    for (let [k, v] of this.userDetails) {
      result.push(v);
    }

    return result;
  }

  //contract for deleting userDetails
  @call
  deleteUser({ accountId }: { accountId: string }): ContractResponse {
    //it will contain boolean value by fetching the accountId it will return true or false
    let accountExists = this.userDetails.get(accountId);

    //if account exists then we can get userDetails
    if (accountExists) {
      // it will delete the entry by taking accountId
      this.userDetails.remove(accountId);
      // return success response
      return {
        message: "User deleted Successfully",
        success: true,
      };
    } else {
      //if the userDetails does not exists return the response
      return {
        message: "User Details not present in blockchain",
        success: false,
      };
    }
  }

  //contract for deletingAllUsers
  @call
  deleteAllUsers(): ContractResponse {
    // it will clear the  userDetails map
    this.userDetails.clear();
    // return the success response
    return {
      message: "User deleted Successfully",
      success: true,
    };
  }
}
